#!/usr/bin/env php
<?php

namespace Vwit\CLI;

class MD5
{
     /**
	 * Maak een MD5 hash sleutelwaarde.
	 * @param string $param
	 * @return string
	 */
	public static function create($param)
	{
	    return md5($param);
	}

}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> md5 {value}\nCreate a MD5 hash.\n");

print "\n";
print MD5::create($param);
exit("\n");

#!/usr/bin/env php
<?php

namespace Vwit\CLI;

class HomeServer
{
     const HOSTS_PATH = 'C:\Windows\System32\drivers\etc\hosts';
     //const HOSTS_PATH = 'C:\Windows\System32\drivers\etc\hosts_31-10-2023.bak';

     /**
      * Command mode
      * @var string
      */
     const DNS_MODE = 'dns';

     /**
      * Hosts group tag
      * Parsed as #EDT_HOMESERVER_TOGGLE_BEGIN
      * @var string
      */
     const EDT_TAG = 'edt';

     /**
      * Hosts group tag
      * Parsed as #ELW_HOMESERVER_TOGGLE_BEGIN
      * @var string
      */
     const ELW_TAG = 'elw';

     /**
      * Hosts group tag  VWIT Services
      * Parsed as #SRV_HOMESERVER_TOGGLE_BEGIN
      * @var string
      */
     const SRV_TAG = 'srv';

     /**
      * Hosts group tag
      * Parsed as #VWIT_HOMESERVER_TOGGLE_BEGIN
      * @var string
      */
     const VWIT_TAG = 'vwit';

     /**
      * Hosts group tag
      * Parsed as #STUB_HOMESERVER_TOGGLE_BEGIN
      * @var string
      */
     const STUB_TAG = 'stub';

     /**
      * Hosts group tag
      * Parsed as #MDL_HOMESERVER_TOGGLE_BEGIN
      * @var string
      */
     const MDL_TAG = 'mdl';

     /**
      * Turn homeserver DNS routing to on using internal Windows local DNS hosts file
      * @var string
      */
     const ON_FLAG = '--on';

     /**
      * Turn homeserver DNS routing to off using external DNS servers
      * @var string
      */
     const OFF_FLAG = '--off';

     /**
      * Homeserver DNS status
      * - List all hosts
      * @var string
      */
     const STATUS_FLAG = '--status';

     /**
      * Homeserver DNS status shortcut
      * - List all hosts
      * @var string
      */
     const S_FLAG = '-s';

     /**
      * Hosts group begin marker
      * A hosts group can be prefixed with a TAG
      * @var string
      */
     const START = 'HOMESERVER_TOGGLE_BEGIN';

     /**
     * Hosts group end marker
     * @var string
     */
     const STOP = 'HOMESERVER_TOGGLE_END';

     const HOMESERVER_IP = '192.168.1.15';

     /**
	 * Toggle https dommains to internal DNS and VwitHomeServer 192.168.1.15  and back to public DNS again
	 * Parsing the domains in the hosts file between #{optional TAG_}HOMESERVER_TOGGLE_BEGIN   and #HOMESERVER_TOGGLE_END
	 * @param string $mode  dns
	 * @param string $tag  edt | elw | vwit | stub | mdl
	 * @param string $flag  --on   | --off
	 * @param array $const  self::Constants
	 * @return void CLI output
	 */
     public static function handle($mode,$tag, $flag, $const)
	{
	    $params = self::validate($mode,$tag, $flag, $const);

	    foreach($params->tags as $tag)
	    {
	        $params->tag = $tag;
	        self::process($params);
	    }

	    if(empty($params->tags)) self::process($params);

	    exit("*** READY ***\n");
	}

	/**
	 * Process each line in the hosts file
	 * @param array $params
	 */
	private static function process($params)
	{
	    $hosts = self::getDataURI($params);

	    $count = $hosts['count'];

	    unset($hosts['count']);
	    $lines = $hosts;
	    array_shift($lines);

	    $intern = 0;
	    $extern = 0;

	    foreach ($lines as $line)
	    {
	        if(preg_match("/#[A-Z]{3,4}_[A-Z]+\s?-+/", $line)) continue;
	        (strpos($line, '#')) === false ? $intern++ : $extern++;
	    }

	    $report = 'No '.$params->tag.' hosts are';
	    if($count >0) $report = $count.( $count == 1 ? ' '.$params->tag.' Host is' : ' '.$params->tag.' Hosts are');

	    $message = "*** $report toggled ".strtoupper( ltrim($params->flag, '--'))." in hosts file ***\n\t";
	    if( $params->flag === self::STATUS_FLAG || $params->flag === self::S_FLAG) $message = "$report found in hosts file, $intern served by homeserver, $extern by external servers\n";

	    print $message;
	    print implode("\n\t",$hosts);
	    print("\n\n");
	    print $message;
	    print("\n");
	}

	/**
	 *
	 * @param string $mode
	 * @param string $tag
	 * @param string $flag
	 * @param array $const
	 * @return object $params   [mode, tag, flag]
	 */
	private static function validate($mode,$tag, $flag, $const)
	{
        $params = (object)
        [
            'mode'=>null,
            'tags'=>[],
            'tag'=>null,
            'flag'=>null,
        ];

        //mode
        if(empty($mode)) exit('ERROR: Please provide a valid mode eg. "dns" as first parameter');

        if(! in_array($mode, [self::DNS_MODE])) exit('ERROR: Invalid mode provided');

        $params->mode = $mode;

        //tags
        if(empty($tag)) exit('ERROR: Please provide valid tag or flag');

        if(in_array($tag, [self::ON_FLAG, self::OFF_FLAG, self::STATUS_FLAG, self::S_FLAG]))
        {
            $params->flag = $tag;
            return $params;
        }

        $tags = explode(",", $tag);

        foreach ($tags as $t)
        {
            if(! in_array($t, [self::ELW_TAG, self::EDT_TAG, self::VWIT_TAG, self::STUB_TAG, self::MDL_TAG, self::SRV_TAG])) exit("ERROR: Invalid tag $t provided");
            $params->tags[] = strtoupper($t);
        }

        //flags
        if(! in_array($flag, [self::ON_FLAG, self::OFF_FLAG, self::STATUS_FLAG, self::S_FLAG])) exit('ERROR: Invalid flag provided, allowed are:  --off | --on | --status | -s');

        $params->flag = $flag;

        return $params;
	}

	/**
	 *
	 * @param object $params
	 * @return mixed[]
	 */
	private static function getDataURI($params)
	{
	    $file = file_get_contents(self::HOSTS_PATH);

	    $rows = [];
	    $hosts = [];

	    $rows = explode("\n", $file); //Split the file by each line

	    $length = count($rows);

	    if($length <2) exit('WARNING : no valid line endings in hosts file, missing LF');

	    $toggle = false;
	    $index = 0;
	    $headers = 0;

	    foreach ($rows as &$row)
	    {
	        if(empty($params->tag))
	        {
	            if(strpos( $row, self::START) !== false )
	            {
	                $toggle = true;
	            }
	        }
	        else
	        {
	            if(strpos( $row, '#'.$params->tag.'_'.self::START ) !== false )
	            {
	                $toggle = true;
	            }
	        }

	        if(substr($row, 0, 22) === '#'.self::STOP) $toggle = false;


            if($toggle && $params->flag === self::ON_FLAG  && substr($row, 0,13) === '#'.self::HOMESERVER_IP)
            {
                $row = str_replace('#'.self::HOMESERVER_IP, self::HOMESERVER_IP, $row);
                $hosts[] = $row;
            }

            if($toggle && $params->flag === self::OFF_FLAG && substr($row, 0,12) === self::HOMESERVER_IP)
            {
                $row = str_replace(self::HOMESERVER_IP, '#'.self::HOMESERVER_IP, $row);
                $hosts[] = $row;
            }

            if($toggle && ($params->flag === self::STATUS_FLAG || $params->flag === self::S_FLAG) && ( strpos($row, self::HOMESERVER_IP) !== false || strpos( $row, self::START) !== false ) )
            {
                if(strpos( $row, self::START) !== false)
                {
                    $hosts[] = "\n".str_replace("_TOGGLE_BEGIN", " -------------------------------", $row);
                    $headers++;
                }
                else
                {
                    $hosts[] = $row;
                }


            }

            $index++;

            if($index < $length) $row.= "\n";
	    }

	    if(!file_exists(self::HOSTS_PATH)) line("Could not find windows hosts file");

	    file_put_contents(self::HOSTS_PATH, $rows);

	    $hosts['count'] = count($hosts) - $headers;

	    return $hosts;
	}

}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

/**
 * The operation mode
 * @example dns
 * @var string $mode
 */
$mode = isset($argv[1]) ? $argv[1] : null;

/**
 * The tagged toggle container, will be represented as {UPPERCASE}_   eg.  #VWIT_HOMESERVER_TOGGLE_BEGIN
 * @example edt | elw | vwit | stubs
 * @var string $tag can also be used as flag
 */
$tag = isset($argv[2]) ? $argv[2] : null;

/**
 * The operaton flag
 * @example --on | --off
 * @var string $flag
 */
$flag = isset($argv[3]) ? $argv[3] : null;

$const = (new \ReflectionClass(HomeServer::class))->getConstants();



if( in_array($mode, ['-h', '--help']) )
{
    print("\nUsage:\n\n$> homeserver {mode[dns]} {tag (optional) [ edt | elw | vwit | srv | stub | mdl]} {flag[--on | --off | -s, --status]}\n"
          ."Route the https domains in the windows hosts file between the tags #HOMESERVER_TOGGLE_BEGIN and  #HOMESERVER_TOGGLE_END to the VwitHomeserver and back to public DNS.\n"
          ."Multiple tags can be specified as comma separated string\n\n");
    print_r($const);
    exit("--------------------------------------------\n");
}


print "\n";
print HomeServer::handle($mode,$tag, $flag, $const);
exit("\n");

#!/usr/bin/env php
<?php

namespace Vwit\CLI;

require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

use Ramsey\Uuid\Uuid as UuidFactory;
use Ramsey\Uuid\UuidInterface;

/**
 * 
 * @author Aaldert
 * @see \Ramsey\Uuid\Uuid
 *
 */
class Uuid
{
	/**
	 * Create a UUID4 string
	 * @return string
	 */
	public static function create($type, $name = null): string
	{
	    $type = intval($type);
	    
	    try
	    {
	        switch($type)
	        {
	            case 1:
	                $tail = rand(1000, 9999).'';
	                $time =  time();
	                $nanotime = (int) $time.$tail;
	                print "*** creating uuid1 for MAC $name: and timestamp: $time\n\n";
	                return UuidFactory::uuid1($name, $nanotime)->toString();
	            case 3:
	                $uuid4 = UuidFactory::uuid4();
	                print "*** creating uuid3 for uuid: $uuid4: and name: $name\n\n";
	                return UuidFactory::uuid3($uuid4, $name)->toString();
	            case 4:
	                return UuidFactory::uuid4()->toString();
	            case 5:
	                $uuid4 = UuidFactory::uuid4();
	                print "*** creating uuid5 for uuid: $uuid4: and name: $name\n\n";
	                return UuidFactory::uuid5($uuid4, $name)->toString();
	            default:
	                exit("ERROR: Invalid UUID type provided\n");
	                
	        }
	    }
	    catch (\Exception $e)
	    {
	        exit("ERROR: ".$e->getMessage().", please provide integer or hexadecimal MAC address\n\n");
	    }    
	}
	
	public static function validate($type, $uuid): string
	{
	    $isValid = false;
	    $type = intval($type);
	    
	    switch($type)
	    {
	        case 1:  
	        case 3: 
	        case 5:
	            $isValid = UuidFactory::isValid($uuid);
	            break;
	        case 4:
	            $isValid =  self::isValidUuid4($uuid);
	            break;
	            
	        default:
	            exit("ERROR: Invalid UUID type $type provided\n");
	            
	    }
	    
	    return $isValid ? "*** UUID IS VALID type $type ***" : "*** UUID IS INVALID type $type ***";
	}
	
	private static function isValidUuid4( $uuid ) {
	    
	    if (!is_string($uuid) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid) !== 1)) {
	        return false;
	    }
	    
	    return true;
	}
}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param_1 = isset($argv[1]) ? $argv[1] : 4;
$param_2 = isset($argv[2]) ? $argv[2] : null;
$param_3 = isset($argv[3]) ? $argv[3] : null;

if( in_array($param_1, ['-h', '--help']) ) exit("\n$> uuid {type} {optional:name | uuid} \nCreate or validate a UUID string. \n\t- GENERATE: {integer: type (1,3,4,5)  |  string:name (type 3 and 5) or MAC (type 1) }\n\t- VALIDATE: {integer: type (1,3,4,5)  |  string:uuid } -v | --validate");

if(in_array($param_3, ['-v', '--validate']))
{
    print "\n";
    print Uuid::validate($param_1, $param_2);
    exit("\n");
}

print "\n";
print Uuid::create($param_1, $param_2);
exit("\n");

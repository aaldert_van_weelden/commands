#!/usr/bin/env php
<?php

namespace Vwit\CLI;

class Password
{
    public static function create($length, $unambig, $upper)
	{
	    //var_dump($length, $unambig);

	    if( is_null($length) ) $length = 16;

	    $key_chars = 'ab%cde@fghijkl#mnop23456789qr$stuvwxyzA!BCD&EF(G-HIJKL*MNOPQ)RSTU?VW/XYZ01[2345]6789';
	    if($unambig === true) $key_chars = 'abcdefghjklmn23456789opqrstuvwxyz23456789ABCDEFGHIJKLMNPQRSTUVWXYZ23456789';
	    if($upper === true) $key_chars = strtoupper($key_chars);

        $rand_max  = strlen($key_chars) - 1;
        for ($i = 0; $i < $length; $i++)
        {
            $rand_pos  = rand(0, $rand_max);
            $rand_key[] = $key_chars[$rand_pos];
        }

        $rand_pass = implode($rand_key);
        return ($rand_pass);
    }
}



#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param_3 = isset($argv[3]) ? $argv[3] : null;
$param_2 = isset($argv[2]) ? $argv[2] : null;
$param_1 = isset($argv[1]) ? $argv[1] : null;

$length = is_numeric($param_1) ? $param_1 : null;
$unambig = in_array($param_1, ['-u', '--unambig'])  || in_array($param_2, ['-u', '--unambig']);
$upper = in_array($param_1, ['-c', '--upper'])  || in_array($param_2, ['-c', '--upper'])  || in_array($param_3, ['-c', '--upper']);



if( in_array($param_1, ['-h', '--help']) )exit("\n$> password {length} [-u, --unambig] [-c, --upper]\nCreate a random password. Optional parameters: {integer:length (Default length is 16) | -u , --unambig (only unambigue charachters and digits)} | -c, --upper(uppercase only) \n");





print "\n";
print Password::create($length, $unambig, $upper);
exit("\n");


#!/usr/bin/env php
<?php

namespace Vwit\CLI;


require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

/**
 *
 * @author Aaldert
 *
 */
class SimilarText
{



	/**
	 * Validate
	 * @param string $param
	 * @param string $check
	 */
	public static function validate($param, $check)
	{
	    $percent = 0;

	    similar_text($param, $check, $percent);

	    return $percent;
	}

}



#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

$check = isset($argv[2]) ? $argv[2] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> Match two strings with similartext  {string:value | string:check}\n");

if($param && $check)
{
    print "\n Matching : ";
    print SimilarText::validate($param, $check);
    exit(" %\n");
}

print "ERROR: missing parameters";

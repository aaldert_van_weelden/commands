#!/usr/bin/env php
<?php

namespace Vwit\CLI;


use Lib\WP\PasswordHash;

require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';
require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'lib'.DIRECTORY_SEPARATOR.'WP'.DIRECTORY_SEPARATOR.'class-phpass.php';

/**
 * 
 * @author Aaldert
 *
 */
class WordpressHasher
{

    /**
     * Create
     * @param string $param
     */
	public static function create($param)
	{   
	    $wp_hasher = new PasswordHash( 8, true );
	    
	    return $wp_hasher->HashPassword( trim( $param ) );
	}
	
	

}



#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> wphash [password] \nCreate  {string:value} a wordpress password hash\n");



print "\n";
print WordpressHasher::create($param);
exit("\n");

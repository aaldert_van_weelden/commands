#!/usr/bin/env php
<?php

namespace Vwit\CLI;

class Base64Decode
{
     /**
	 * Decode the provided string.
	 * @param string $param
	 * @return string
	 */
	public static function create($param)
	{
	    return base64_decode($param);
	}

}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> base64decode {value}\nDecode provided base64 string.\n");

print "\n";
print Base64Decode::create($param);
exit("\n");

#!/usr/bin/env php
<?php

namespace Vwit\CLI;

class Img2base64
{
     /**
	 * Convert the provided image to a base64 decoded HTML image source
	 * @param string $param The file path
	 * @return string
	 */
	public static function create($param)
	{
        $file = self::getDataURI($param);

        print "\n";
        print "Base64 decoded image has been written into file : ".$file;
	}

	private static function getDataURI($imagePath)
	{
	    $finfo = new \finfo(FILEINFO_MIME_TYPE);
	    $type = $finfo->file($imagePath);

	    $data = 'data:' . $type . ';base64,' . base64_encode(file_get_contents($imagePath));

	    $parts = explode("\\",$imagePath);
	    $filename = array_pop($parts);
	    $logfile = realpath(__DIR__."/../")."\output\\".$filename.".code";

	    file_put_contents($logfile, $data);

	    return $logfile;


	}

}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> md5 {value}\nConvert the provided image to base64 imagesource.\n");

print "\n";
print Img2base64::create($param);
exit("\n");

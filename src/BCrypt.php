#!/usr/bin/env php
<?php

namespace Vwit\CLI;

use Illuminate\Hashing\BcryptHasher;

require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

class BCrypt
{

     /**
	 * Maak een BCrypt hash sleutelwaarde.
	 * @param string $param
	 * @return string
	 */
	public static function create($param)
	{
	    return (new BcryptHasher())->make($param);
	}
	
	/**
	 * Valideer een BCrypt hash sleutelwaarde.
	 * @param string $param
	 * @return string
	 */
	public static function validate($param, $hash)
	{
	    return (new BcryptHasher())->check($param, $hash)? "*** HASH IS VALID ***" : "*** HASH IS INVALID ***";
	}

}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

$hash = isset($argv[2]) ? $argv[2] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> bcrypt {value} {hash}\nCreate a BCrypt hash {string:value} or validate a hash {string:value | string:hash}\n");

if($param && $hash)
{
    print "\n";
    print BCrypt::validate($param, $hash);
    exit("\n");
}

print "\n";
print BCrypt::create($param);
exit("\n");

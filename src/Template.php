#!/usr/bin/env php
<?php

namespace Vwit\CLI;


require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

/**
 * 
 * @author Aaldert
 *
 */
class Template
{

    /**
     * Create
     * @param string $param
     */
	public static function create($param)
	{
	    return 'Implement '.__FUNCTION__;
	}
	
	/**
	 * Validate
	 * @param string $param
	 * @param string $check
	 */
	public static function validate($param, $check)
	{
	    return 'Implement '.__FUNCTION__;
	}

}



#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

$check = isset($argv[2]) ? $argv[2] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> template \nCreate  {string:value} or validate  {string:value | string:check}\n");

if($param && $check)
{
    print "\n";
    print Template::validate($param, $check);
    exit("\n");
}

print "\n";
print Template::create($param);
exit("\n");

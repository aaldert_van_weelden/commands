#!/usr/bin/env php
<?php

namespace Vwit\CLI;


require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

/**
 *
 * @author Aaldert
 *
 */
class ListAlias
{

    /**
     * Create
     * @param string $param
     */
	public static function create($param)
	{
	    $hidden = ['info.txt','.','..','...','list'];
	    $basepath = realpath(__DIR__.'/..');
	    $path = $basepath.DIRECTORY_SEPARATOR.'alias'.DIRECTORY_SEPARATOR;
	    $fileList = scandir($path);

	    print "\n-------------- known custom alias-----------------------------------\n\n\n";

	    array_map(function($filename) use($basepath, $path, $hidden)
	    {
	        $filename = str_replace( '.bat' , '', $filename);


	        if(in_array($filename, $hidden)) return false;

	        print str_pad($filename, 10);
	        print ' => ';

	        print str_replace('REM', '', file($path.$filename.'.bat')[0]);
	        print "\n";

	        return true;

	    },
	        $fileList
	    );

	    print "\n--------------------------------------------------------------\n\n";
	    return;
	}

}



#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> listalias \nList all available custom aliases\n");


print "\n";
print ListAlias::create($param);
exit("\n");

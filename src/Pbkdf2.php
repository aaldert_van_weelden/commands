#!/usr/bin/env php
<?php

namespace Vwit\CLI;


require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

/**
 *
 * @author Aaldert
 *
 */
class Pbkdf2 {




    /**
     * Create
     * @param string $param
     * @param string $algo
     */
    public static function create($password, $salt, $algo = 'sha512', $iterations = 20000, $key_length = 128, $raw_output = false)
    {
        return self::pbkdf2($password, $salt, $algo, $iterations, $key_length, $raw_output);

        //return hash_pbkdf2($algo,$password,$salt,$iterations,$key_length,$raw_output);
    }





    private static function pbkdf2($password, $salt, $algorithm = 'sha512', $count = 20000, $key_length = 128, $raw_output = false)
    {
        if(!in_array($algorithm, hash_algos(), true)) {
            exit('pbkdf2: Hash algoritme is niet geinstalleerd op het systeem.');
        }

        if($count <= 0 || $key_length <= 0) {
            $count = 20000;
            $key_length = 128;
        }

        $hash_length = strlen(hash($algorithm, "", true));
        $block_count = ceil($key_length / $hash_length);

        $output = "";
        for($i = 1; $i <= $block_count; $i++) {
            $last = $salt . pack("N", $i);
            $last = $xorsum = hash_hmac($algorithm, $last, $password, true);
            for ($j = 1; $j < $count; $j++) {
                $xorsum ^= ($last = hash_hmac($algorithm, $last, $password, true));
            }
            $output .= $xorsum;
        }

        if($raw_output) {
            return substr($output, 0, $key_length);
        }
        else {
            return base64_encode(substr($output, 0, $key_length));
        }
    }


}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

$salt = isset($argv[2]) ? $argv[2] : null;

if( in_array($param, ['-h', '--help']) )
{
    print("\n$> pbkdf2 {value} {algorithm}\nCreate a hash with the provided algorithm {string:value | string:salt}\nAvailable algorithms are:\n\n");
    $algos = hash_algos();
    foreach($algos as $algo)
    {
        print "\t".$algo."\n";
    }
    exit("\n");
}

if($param && $salt)
{
    print "\n";
    print Pbkdf2::create($param, $salt);
    exit("\n");
}

print "\n";
print 'Please provide both a string:value and a string:salt';
exit("\n");

#!/usr/bin/env php
<?php

namespace Vwit\CLI;


error_reporting(E_ALL);

class Vhost
{
     const HOSTS_PATH_LOCAL = 'C:\wamp64\bin\apache\apache2.4.54.2\conf\extra\httpd-vhosts.conf';
     const HOSTS_PATH_HOMESERVER = 'todo: configure this';

     /**
      * Command mode
      * @var string
      */
     const LIST_HOSTNAME_AND_PATH = 'list';

     const OPTION_LOCAL = '--local';

     const OPTION_HOMESERVER = '--homeserver';

     const OPTION_LOCAL_SC = '-l';

     const OPTION_HOMESERVER_SC = '-s';

     const HOMESERVER_IP = '192.168.1.15';

     private static $currentroot =null;

     /**
	 * Display local vhosts as defined in WAMP Server
	 *
	 * @param string $mode  list
	 * @param string $flag  --homeserver
	 * @param string $query  text to search for in hostnames
	 * @return void CLI output
	 */
     public static function handle($mode,$flag,$query)
	{
	    try
	    {
	        $params = self::validate($mode, $flag, $query);

	        self::process($params);

	        exit("*** READY ***\n");
	    }
	    catch (\Exception $e)
	    {
	        exit('ERROR: '.$e->getMessage());
	    }
	}

	/**
	 * Process each line in the hosts file
	 * @param array $params
	 */
	private static function process($params)
	{
	    $hosts = self::getVirtualHosts($params);

	    $virtualhosts = [];
	    $output = [];


	    foreach ($hosts as $entry)
	    {
	        if(strpos($entry, 'DocumentRoot') !== false)
	        {
	            if(is_null(self::$currentroot)) self::$currentroot = $entry;
	        }

	        if(strpos($entry, 'ServerName') !== false)
	        {
	            $key = trim(str_replace('ServerName', '', $entry));

	            $virtualhosts[$key] = self::$currentroot;
	            self::$currentroot = null;
	        }
	    }

	    foreach ($virtualhosts as $name=>$root)
	    {
	        $has_query = !empty($params->query);
	        $is_found = $has_query && (strpos($name, $params->query) !== false || strpos($root, $params->query) !== false);

	        if($has_query && !$is_found) continue;

	        $output[] = str_pad($name, 40 ,'.').$root;
	    }

	    sort($output);

	    print implode("\n", $output)."\n\n";

	}

	/**
	 *
	 * @param string $mode
	 * @param string $flag
	 * @param string $query
	 * @throws \ErrorException
	 * @return object properties[mode,flag,query]
	 */
	private static function validate($mode, $flag, $query)
	{
	    if($mode != self::LIST_HOSTNAME_AND_PATH) throw new \Exception('Please provide a valid mode, for now only [list] is available');

	    if(strpos($flag, '-q') !== false)
	    {
	        $query = $flag;
	        $flag = self::OPTION_LOCAL;

	    }

	    if(! in_array($flag, [self::OPTION_HOMESERVER, self::OPTION_HOMESERVER_SC, self::OPTION_LOCAL, self::OPTION_LOCAL_SC])) throw new \Exception('Please provide a valid flag [--local (default) | -l | --homeserver | -s]');

	    if($flag == self::OPTION_HOMESERVER_SC) $flag = self::OPTION_HOMESERVER;
	    if($flag == self::OPTION_LOCAL_SC) $flag = self::OPTION_LOCAL;

	    if(empty($query)) $query = '';
	    if(!empty($query)) $query = str_replace(['-q=','--query='], '', $query);

	    return (object) ['mode'=>$mode, 'flag'=>$flag, 'query'=>$query];
	}


	/**
	 *
	 * @param object $params
	 * @return mixed[]
	 */
	private static function getVirtualHosts($params)
	{
	    $hostpath = $params->flag == self::OPTION_LOCAL ? self::HOSTS_PATH_LOCAL : self::HOSTS_PATH_HOMESERVER;

	    if(!file_exists($hostpath)) throw new \ErrorException("Could not find windows hosts file ".$hostpath);

	    $file = file_get_contents($hostpath);

	    $rows = [];
	    $hosts = [];

	    $rows = explode("\n", $file); //Split the file by each line

	    foreach ($rows as $row)
	    {
	        $is_virtualhost = strpos($row, 'DocumentRoot') !== false || strpos($row, 'ServerName') !== false;
	        $is_comment = strpos($row, '#') !== false;

	        if($is_virtualhost && !$is_comment )
	        {
	            $hosts[] = $row;
	        }
	    }

	    $hosts['count'] = count($hosts);

	    return $hosts;
	}

}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

/**
 * The operation mode
 * @example list
 * @var string $mode
 */
$mode = isset($argv[1]) ? $argv[1] : null;

/**
 * The operaton flag
 * @example --local, -l | --homeserver, -s
 * @default local
 * @var string $flag
 */
$flag = isset($argv[2]) ? $argv[2] : '--local';

$query = isset($argv[3]) ? $argv[3] : null;



if( in_array($mode, ['-h', '--help']) )
{
    print("\nUsage:\n\n$> Vhost {mode[list]}  {flag[--local, -l (default | --homeserver, -s | -q={string}, --query={string}]}\n"
          ."Display each local or homeserver available VHOST name and documentroot\n"
          ."Output can be filtered by providing a searchquery string\n\n");
    exit("--------------------------------------------\n");
}

print "\n";
print Vhost::handle($mode,$flag,$query);
exit("\n");

#!/usr/bin/env php
<?php

namespace Vwit\CLI;

class Timestamp
{
     /**
	 * Maak een MD5 hash sleutelwaarde.
	 * @param string $param
	 * @return string
	 */
	public static function create($param_1, $param_2)
	{
	    date_default_timezone_set('Europe/Amsterdam');

	    if(is_null($param_1)) return time();

	    if(is_null($param_2)) return strtotime($param_1);

	    if($param_2 !== '-t') return "Invalid parameter $param_2, do you mean -t ?";

	    $date = new \DateTime();
	    $date->setTimestamp($param_1);

	    return $date->format('Y-m-d H:i:s');

	}

}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;


$param_1 = isset($argv[1]) ? $argv[1] : null;
$param_2 = isset($argv[2]) ? $argv[2] : null;

if( in_array($param_1, ['-h', '--help']) ) exit("\n$> timestamp {\"value\":optional(timestamp or datetime)} {-t:optional. Convert timestamp to datetime} \nCreate a timestamp from datetime doublequote enclosed string.\nDates in the m/d/y or d-m-y formats are disambiguated by looking at the separator between the various components: \nif the separator is a slash (/), then the American m/d/y is assumed; \nwhereas if the separator is a dash (-) or a dot (.), then the European d-m-y format is assumed.");

print "\n";
print Timestamp::create($param_1, $param_2);
exit("\n");

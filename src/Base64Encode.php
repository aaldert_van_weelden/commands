#!/usr/bin/env php
<?php

namespace Vwit\CLI;

class Base64Encode
{
     /**
	 * Encode the provided string.
	 * @param string $param
	 * @return string
	 */
	public static function create($param)
	{
	    return base64_encode($param);
	}

}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> base64encode {value}\nEncode input string to base64.\n");

print "\n";
print Base64Encode::create($param);
exit("\n");

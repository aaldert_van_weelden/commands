#!/usr/bin/env php
<?php

namespace Vwit\CLI;


require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

/**
 * 
 * @author Aaldert
 *
 */
class Hash
{

    /**
     * Create
     * @param string $param
     * @param string $algo
     */
	public static function create($param, $algo)
	{
	    return hash($algo, $param);
	}
	
}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

$algo = isset($argv[2]) ? $argv[2] : null;

if( in_array($param, ['-h', '--help']) )
{
    print("\n$> hasher {value} {algorithm}\nCreate a hash with the provided algorithm {string:value | string:algorithm}\nAvailable algorithms are:\n\n");
    $algos = hash_algos();
    foreach($algos as $algo)
    {
        print "\t".$algo."\n";
    }
    exit("\n");
}

if($param && $algo)
{
    print "\n";
    print Hash::create($param, $algo);
    exit("\n");
}

print "\n";
print 'Please provide both a string:value and a string:algorithm';
exit("\n");

#!/usr/bin/env php
<?php

namespace Vwit\CLI;


require realpath(__DIR__.'/..').DIRECTORY_SEPARATOR.'vendor'.DIRECTORY_SEPARATOR.'autoload.php';

/**
 *
 * @author Aaldert
 *
 */
class ListCustomCommands
{

    /**
     * Create
     * @param string $param
     */
	public static function create($param)
	{
	    $hidden = ['wp','.','..','...','mysqlstart','mysqlstop'];
	    $basepath = realpath(__DIR__.'/..');
	    $path = $basepath.DIRECTORY_SEPARATOR.'bin'.DIRECTORY_SEPARATOR;
	    $fileList = scandir($path);

	    print "\n-------------------------------------------------------------------------------------------------------------------\n";

	    array_map(function($filename) use($basepath, $hidden)
	    {
	        $filename = str_replace( '.exe' , '', $filename);

	        if(in_array($filename, $hidden)) return false;

	        system($basepath.DIRECTORY_SEPARATOR.'bat'.DIRECTORY_SEPARATOR."$filename.bat -h", $op);
	        print "\n-------------------------------------------------------------------------------------------------------------------\n";

	        return true;

	    },
	        $fileList
	    );

	    print "\n$> mysqlstart Start MySQL server instance at port 3306 \n";
	    print "\n-------------------------------------------------------------------------------------------------------------------\n";

	    print "\n$> mysqlstop  Stop MySQL server instance at port 3306  \n";
	    print "\n-------------------------------------------------------------------------------------------------------------------\n";

	    print "\n$> wp Wordpress CLI see https://developer.wordpress.org/cli/commands/ \n";
	    print "\n-------------------------------------------------------------------------------------------------------------------\n";

	    return;
	}

}



#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$param = isset($argv[1]) ? $argv[1] : null;

if( in_array($param, ['-h', '--help']) ) exit("\n$> listcustomcmd \nList all available custom commands with help text\n");


print "\n";
print ListCustomCommands::create($param);
exit("\n");

#!/usr/bin/env php
<?php

namespace Vwit\CLI;

class Serve
{
     /**
	 * Start een http dev server in de aangegeven directory.
	 * @param string $param
	 * @return string
	 */
	public static function handle($dir, $host, $port)
	{
        chdir('C:/Users/Aaldert/git/'.$dir);

        if( empty($port)) $port = "8000";
        if( empty($host)) $host = "localhost";

        $command = "php artisan serve --host $host --port $port 2>&1";


        $output = [];
        $returntVal = 0;

        exec($command, $output, $returnVal);

        print_r($output);

        return $returntVal;
	}

}

#-------------------------------------------------------------------------------------------------
# Execute command
#-------------------------------------------------------------------------------------------------

global $argv;

$dir = isset($argv[1]) ? $argv[1] : null;

$host = isset($argv[2]) ? $argv[2] : null;

$port = isset($argv[3]) ? $argv[3] : null;

if( in_array($dir, ['-h', '--help']) ) exit("\n$> serve {gitrepo dirname} {hostname|localhost} {port|8000} Serve the Artisan project in the provided ~/git/{dirname} directory. \n\n");

print "\n";
print Serve::handle($dir, $host, $port);
exit("\n");

# Define Commands using PHP#

Used to implement php scripts running in CLI on Windows

### Windows path and variable  ###

* Install the commands package
* Place the .bat files in the corresponding `/bat` directory
* Compile your executables into your preferred `/bin` directory by using Advanced BAT to EXE  (`https://www.battoexeconverter.com`)
* Remove the `.exe` extension when the commands are run as  CLI Cygwin bash commands (Featuring autocompletion). For windows CLI commands leave the `.exe` intact
* Add a `PHP_CLI_COMMANDS` variable pointing to the package PHP classfiles source directory,  for example `{user}\commands\src` 
* Add a windows environment path to the `bin`directory where you have put the executables, eg `{user\commands\bin}`

### Conversion from .bat to .exe ###

* create `my_command.php` script (possible without .php extention)
* execute php script in `my_command.bat` file
* Compile with Advanced BAT to EXE  (see `https://www.battoexeconverter.com`)
* 

### Help documantation

See the Template class for the proper help `-h` implementation, the `listcustomcmd` command displays an overview of the available commands

